INCLUDE Ink/0_Ink_Menu.ink
INCLUDE LInk/0_LInk_Menu.ink
INCLUDE Ink/1_Basics/3_Knots.ink
INCLUDE Ink/1_Basics/4_Diverts.ink
INCLUDE Ink/1_Basics/5_Branch_The_Flow.ink
INCLUDE Ink/1_Basics/1_Content.ink
INCLUDE Ink/1_Basics/2_Choices.ink
INCLUDE Ink/1_Basics/0_Basics_Menu.ink
INCLUDE Ink/1_Basics/6_Includes_And_Stitches.ink





-> start



== start ==
>>> play_music awakening_music
>>> change_room courtyard
>>> change_layout layout_intimate Lana

Lana (happy): Hi! I'm Lana. I will help you to learn how to use the ink scripting language, and also LInk, an ink extension created for the game Connect.
    The best way to use this tutorial is to follow the ink code at the same time.
-> Tutorial_Menu

=== init ===
// Need it for compatibilty with LInk
VAR affinity_lana = 0
-> DONE

== Tutorial_Menu ==
Lana (neutral): Do you want to learn about Ink or LInk ?
    + [I want to learn about Ink.] -> Ink_Menu
    + [I want to learn about LInk.] -> LInk_Menu
    + [I want to leave please.]
-> END
