== Include_And_Stitches ==
Lana: Let's talk about including multiple files and stitches.
    + [Knots can be subdivided] -> knots_can_be_subdivided
    + [Stitches have unique names] -> Stitches_have_unique_names
    + [The first stitch is the default] ->The_First_Stitch_Is_The_Default
    + [Local diverts] -> Local_Diverts
    + [Script files can be combined] -> Script_Files_Can_Be_Combined
    + [Come back to basics] -> Basics_Menu
    
= knots_can_be_subdivided
Lana: As stories get longer, they become more confusing to keep organised without some additional structure.
    Knots can include sub-sections called "stitches". These are marked using a single equals sign. Look the code to see all the stitches in this file!
    One could use a knot for a scene, for instance, and stitches for the events within the scene.
    -> Include_And_Stitches

= Stitches_have_unique_names
Lana: A stitch can be diverted to using its "address". Check the code!
-> Include_And_Stitches.my_stitch

= my_stitch
Lana: I've just divert using the address of this stitch!
-> Include_And_Stitches

= The_First_Stitch_Is_The_Default
Lana: Diverting to a knot which contains stitches will divert to the first stitch in the knot.
    Check the code!
-> my_parent_knot

= The_First_Stitch_Is_The_Default_2
Lana: You can also include content at the top of a knot outside of any stitch. However, you need to remember to divert out of it - the engine won't automatically enter the first stitch once it's worked its way through the header content.
    Look at the text just below the Include_And_Stitches knot.
-> Include_And_Stitches

= Local_Diverts
Lana: From inside a knot, you don't need to use the full address for a stitch. See all the local diverts int this file!
Lana: This means stitches and knots can't share names, but several knots can contain the same stitch name. (So both the Orient Express and the SS Mongolia can have first class.)

Lana: The compiler will warn you if ambiguous names are used.
-> Include_And_Stitches

= Script_Files_Can_Be_Combined
Lana: You can also split your content across multiple files, using an include statement. Look at Tutorial.ink file, it includes all other files!
Lana: Include statements should always go at the top of a file, and not inside knots.

Lana: There are no rules about what file a knot must be in to be diverted to. (In other words, separating files has no effect on the game's namespacing).
-> Include_And_Stitches

=== my_parent_knot ===
= my_first_stitch
Lana: I've just diverted to my_parent_knot, but ended up in its first stitch!
-> Include_And_Stitches.The_First_Stitch_Is_The_Default_2

