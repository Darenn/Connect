== Diverts ==
Lana: You can tell the story to move from one knot to another using \-\>, a "divert arrow". Diverts happen immediately without any user input (check the code!)
-> diverts_are_invisible // divert to a knot

== diverts_are_invisible ==
Lana: Diverts are intended to be seamless and can even happen mid-sentence (check the code!).
-> hurry_home

=== hurry_home ===
lana: We hurried home to Savile Row -> as_fast_as_we_could

=== as_fast_as_we_could ===
as fast as we could.
Lana: Seen how it outputs only one line?
-> Glue

== Glue ==
Lana: The default behaviour inserts line-breaks before every new line of content. In some cases, however, content must insist on not having a line-break, and it can do so using \<\>, or "glue". Check the code!
-> hurry_home_2

=== hurry_home_2 ===
We hurried home <> 
-> to_savile_row

=== to_savile_row ===
to Savile Row 
-> as_fast_as_we_could_2

=== as_fast_as_we_could_2 ===
<> as fast as we could.
Lana: Seen how it produces one line again?
Lana: You can't use too much glue: multiple glues next to each other have no additional effect. (And there's no way to "negate" a glue; once a line is sticky, it'll stick.)
-> Basics_Menu