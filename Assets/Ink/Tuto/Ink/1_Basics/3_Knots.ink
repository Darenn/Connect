== Knots ==
Lana (happy): Pieces of content are called knots.
    To allow the game to branch we need to mark up sections of content with names (as an old-fashioned gamebook does with its 'Paragraph 18', and the like.)
    These sections are called "knots" and they're the fundamental structural unit of ink content.
    -> Here_is_a_knot

== Here_is_a_knot
Lana (neutral): The start of a knot is indicated by two or more equals signs (check the code!)
-> Another_knot

== Another_knot ==
Lana: The equals signs on the end are optional; and the name needs to be a single word with no spaces.
    The start of a knot is a header; the content that follows will be inside that knot.

Lana: When you start an ink file, content outside of knots will be run automatically. But knots won't.
    We do this with a divert arrow \-\>, which is covered properly in the next section.
    -> end_the_story
    
== end_the_story
Lana: However, ink doesn't like loose ends, and produces a warning on compilation and/or run-time when it thinks this has happened.
Lana: \-\> END is a marker for both the writer and the compiler; it means "the story flow should now stop".
Lana: Look at the \-\> END in the file Tutorial.ink. If you follow all the possible story flows of this tutorial, you'll see that they all ultimately finish on this end marker. 
-> Basics_Menu
