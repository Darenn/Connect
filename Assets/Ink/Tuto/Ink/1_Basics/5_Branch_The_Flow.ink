== Branching_The_Flow ==

= Basic_Branching
Lana (happy): Combining knots, choices and diverts gives us the basic structure of a choose-your-own game. Look a this example (code!).
-> paragraph_1

=== paragraph_1 === 
Des: You stand by the wall of Analand, sword in hand.
* [Open the gate] -> paragraph_2 
* [Smash down the gate] -> paragraph_3
* [Turn back and go home] -> paragraph_4

=== paragraph_2 ===
Des: You open the gate, and step out onto the path.
-> Branching_and_joining

=== paragraph_3 ===
Des: You smash down the gate, and step onto the path.
-> Branching_and_joining

=== paragraph_4 ===
Des: You give up, turn back and go home.
-> Branching_and_joining

=== Branching_and_joining
Lana: Using diverts, the writer can branch the flow, and join it back up again, without showing the player that the flow has rejoined. Look at this example (code!).
-> back_in_london

=== back_in_london ===

Des: We arrived into London at 9.45pm exactly.

*	Des: "There is not a moment to lose!"[] I declared.
	-> hurry_outside 
	
*	Des: "Monsieur, let us savour this moment!"[] I declared.
	Des: My master clouted me firmly around the head and dragged me out of the door. 
	-> dragged_outside 

*	[We hurried home] -> hurry_outside

	
=== hurry_outside ===
Des: We hurried home to Savile Row -> as_fast_as_we_could_branching


=== dragged_outside === 
Des: He insisted that we hurried home to Savile Row 
-> as_fast_as_we_could_branching


=== as_fast_as_we_could_branching === 
<> as fast as we could.
-> The_Story_Flow

= The_Story_Flow
Lana: Knots and diverts combine to create the basic story flow of the game. This flow is "flat" - there's no call-stack, and diverts aren't "returned" from.

Lana: In most ink scripts, the story flow starts at the top, bounces around in a spaghetti-like mess, and eventually, hopefully, reaches a \-\> END.

Lana: The very loose structure means writers can get on and write, branching and rejoining without worrying about the structure that they're creating as they go. There's no boiler-plate to creating new branches or diversions, and no need to track any state.
-> Basics_Menu