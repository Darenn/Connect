== Content ==
Lana (happy): How to write paragraphs, leave comments in your story, and use tags.
    + [Paragraphs] -> Paragraphs
    + [Comments] -> Comments
    + [Tags] -> Tags
    + [Come back to basics] -> Basics_Menu
    
= Paragraphs
Lana (happy): Ink displays text paragraph per paragraph.
        This is a *paragraph*.
        Text on separate lines produces new paragraphs.
-> Content

= Comments
Lana (happy): By default, all text in your file will appear in the output content, unless specially marked up.

Lana (neutral): Check out the ink code to see the two types of comments : One-Line Comments, and Multi-Line Comments.

// This is a one-line comment
Lana: You can use "\/\/" to make a one-line comment. 

/*
This is a
multi-line comment
*/
Lana: But also \/\* ... \*\/ to make a multi-line comment.

TODO: This is a todo example, nothing to worry mate.
Lana: You can also use the TODO keyword to remind you of you need to do. It will be printed at compilation time.
-> Content

= Tags
Lana (happy): Text content from the game will appear 'as is' when the engine runs. However, it can sometimes be useful to mark up a line of content with extra information to tell the game what to do with that content.

    Ink provides a simple system for tagging lines of content, with hashtags (check the code!) # colour it blue
    
    These don't show up in the main text flow, but can be read off by the game and used as you see fit.
-> Content