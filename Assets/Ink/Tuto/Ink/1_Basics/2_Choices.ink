== Choices ==
Lana (neutral): How to create a Text Choice, Supressing choice text and creating multiple Text Choices.
    + [Create a Text Choice] -> Text_Choice
    + [Supressing choice text] -> Supressing_Choice_Text
    + [Create multiple choices] -> Multiple_Choices
    + [Get back to basics.] -> Basics_Menu

= Text_Choice
Lana (happy): Input is offered to the player via text choices. A text choice is indicated by an * character in Ink code.
Lana: Here is an example choice.
    * Des: This is a choice!
    Lana (happy): Glad you clicked on the choice!
Lana (neutral): If no other flow instructions are given, once made, the choice will flow into the next line of text (like this one).
Lana: By default, the text of a choice appears again, in the output, but it can be supressed.
-> Choices

= Supressing_Choice_Text
Lana (happy): Some games (like Connect) separate the text of a choice from its outcome. In ink, if the choice text is given in square brackets, the text of the choice will not be printed into response.
Lana: Click on the choice!
    * Des: This is my choice! -> Second_Choice
= Second_Choice
Lana: As you can see your choice was writen on the output.
Lana: Click on this one please!
    * [Des: This is my non repeated choice!]-> Mixing_choice_and_output_text

= Mixing_choice_and_output_text
Lana: As you can see your choice was not writen this time!
Lana: You can actually mix choice and output text.
Lana: The square brackets in fact divide up the option content. What's before is printed in both choice and output; what's inside only in choice; and what's after, only in output. Effectively, they provide alternative ways for a line to end.
Lana: Hello world!
*	Des: Hello [back!] right back to you!
	Lana: Nice to hear from you!
Lana: As you can see, only the text outside the brackets was repeated.
    It's a very neat feature when writing short and organized ink dialogues.
-> Choices

= Multiple_Choices
Lana (thoughtful): To make choices really choices, we need to provide alternatives. 
Lana (Happy): We can do this simply by listing them!
    * Des: Oh that's great! -> End_Multiple_Choice
    * Des: It's not that great. -> End_Multiple_Choice
= End_Multiple_Choice
Lana (neutral): The above syntax is enough to write a single set of choices. In a real game, we'll want to move the flow from one point to another based on what the player chooses. To do that, we need to introduce a bit more structure: the knots.
-> Choices