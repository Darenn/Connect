﻿=== init ===
VAR affinity_amaryllis = 0
VAR affinity_lana = 0
VAR affinity_lucian = 0
VAR affinity_kendall = 0
VAR affinity_noah = 0
VAR affinity_charlie = 0
-> DONE

=== start ===
>>> change_room courtyard
-> lana_talk
-> DONE

== lana_talk ==
Lana (sad): Yo j'ai trop la dalle
-> frite
= frite
Lana (happy): J'ai toujours aimé les frites!
Lana (neutral): Et toi tu aimes ça?
    * Player: Oui! $> increase_affinity lana 1
        Lana (happy): Super, moi aussi!
    * Player: Non...
        Lana (annoyed): Oh ok je vois.
    --> football
= football
Lana (happy): Mais le foot c'est super cool!
Lana (neutral): Tu penses pas?
    * Player: Ouais c'est juste trop bien quoi! $> increase_affinity lana 1
        Lana (happy): Mais tellement!
    * Player: C'est quoi?
        Lana (sad): Euh ok... laisse tomber! $> increase_affinity lana -1
    --> DONE

== quest1 ==
Lana (neutral): Je ne sais pas quoi porter aujourd'hui, tu peux m'aider?
    + Player: Une jupe sexy. $> increase_affinity lana 1
        Lana (happy): Han mais ouais super idée putain! Merci!
    + Player: Un pentalon jean classique. 
        Lana (neutral): Hum... je ne pense pas, je vais chercher, merci de ton aide.
    + Player: Ne portes rien! $> increase_affinity lana -1
        Lana (annoyed): Mais t'es vraiment con putain.
    --> DONE

== quest2 ==
Amaryllis: Coucou... euh...
Amaryllis: Je crois avoir vu quelque chose bouger sous le coussin...
Amaryllis: Tu peux regarder?
    + Player: C'est bien trop dangereux, je ne m'y risque pas. $> increase_affinity Amaryllis -1
    + Player: Oui bien sûr. $> increase_affinity Amaryllis 1
        Amaryllis: Merci...
        Player: AH MAIS CEST LA GROSSE DICK DE DARENN.
    --> DONE

== lana_ask ==
Lana: What do you want?
-> DONE

== amaryllis_ask ==
amaryllis: Need something...?
-> DONE

== amaryllis_talk ==
amaryllis: I'm fine thanks.
-> DONE