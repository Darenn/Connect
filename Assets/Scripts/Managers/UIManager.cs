﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private CalendarUI calendarUI = default;
	[SerializeField] private Button buttonPrefab = default;
	[SerializeField] private GameObject navigationPanel = default;
	[SerializeField] private GameObject roomsContainer = default;

    private int oldDayNumber = 1;

	void Start() {
		for (int i = 0; i < roomsContainer.transform.childCount; i++)
		{
			CreateButton(roomsContainer.transform.GetChild(i).name, i);
		}
	}

	void CreateButton(string name, int index) {
		createNavigationButton(name, delegate {OnRoomButtonClicked(index);});
	}

    public void DisplayCalendar(Calendar calendar, Day newDay) {
        calendarUI.gameObject.SetActive(true);
        calendarUI.DisplayCalendar(calendar, 0, oldDayNumber, newDay.DayNumber);
        oldDayNumber = newDay.DayNumber;
    }

    public void HideCalendar() {
        calendarUI.gameObject.SetActive(false);
    }

	private Button createNavigationButton(string roomName, UnityEngine.Events.UnityAction callback) {
			Button choiceButton = Instantiate (buttonPrefab);
			choiceButton.transform.SetParent (navigationPanel.transform, false);
			
			// Gets the text from the button prefab
			TMP_Text choiceText = choiceButton.GetComponentInChildren<TMP_Text> ();
			choiceText.text = roomName;

			// Make the button expand to fit the text
			HorizontalLayoutGroup layoutGroup = choiceButton.GetComponent <HorizontalLayoutGroup> ();
			layoutGroup.childForceExpandHeight = false;
            choiceButton.onClick.AddListener (callback);
            return choiceButton;
    }

	private void OnRoomButtonClicked(int index) {
		GameManager.Instance.ChangeRoom(roomsContainer.transform.GetChild(index).name);
	}
}
