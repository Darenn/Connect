﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

/*
* Manage all UI for dialogs (layouts and actors)
 */
public class DialogUIManager :  Singleton<DialogUIManager>
{
	[HideInInspector] public DialogLayout CurrentLayout;

	[SerializeField] private GameObject actorContainer = default;
	[SerializeField] private GameObject layoutContainer = default;
	public TMP_InputField InputField = default;

	private Actor[] actors;
	private DialogLayout[] layouts;

	void Awake() {
		actors = new Actor[actorContainer.transform.childCount];
		for (int i = 0; i < actors.Length; i++)
		{
			actors[i] = actorContainer.transform.GetChild(i).GetComponent<Actor>();
		}
		layouts = new DialogLayout[layoutContainer.transform.childCount];
		for (int i = 0; i < layouts.Length; i++)
		{
			layouts[i] = layoutContainer.transform.GetChild(i).GetComponent<DialogLayout>();
		}
		CurrentLayout = layouts[0];

		GameManager.OnDialogEnd += OnStopDialog;
	}

	void Start() {
		// Deactivating at awake stop their awake
		for (int i = 0; i < actors.Length; i++)
		{
			actors[i].gameObject.SetActive(false);
		}
		for (int i = 0; i < layouts.Length; i++)
		{
			layouts[i].gameObject.SetActive(false);
		}
	}

    public Button CreateChoiceButton(string choice, UnityEngine.Events.UnityAction callback) {
		return CurrentLayout.CreateChoiceButton(choice, callback);
    }

	public void DisplayText(string text, string actorName, string actorHumor) {
		if(!CurrentLayout.gameObject.activeSelf) {
			CurrentLayout.gameObject.SetActive(true);
		}
		Actor actor = getActor(actorName);
		if (!actor)  {
			Debug.LogError("No actor : '" + actorName.ToLower() + "'.");
			return;
		}
		if (actorHumor != "") actor.Humor = actorHumor;
		if (actorName.ToLower() == "des" || actorName.ToLower() == "desc") {
			CurrentLayout.DisplayDescText(text);
		} else {
			CurrentLayout.DisplayText(text, actor);
		}
	}

	public void OnStopDialog() {
		CurrentLayout.Clear();
		CurrentLayout.gameObject.SetActive(false);
	}

	public void ChangeLayout(string layoutName) {
		CurrentLayout.Clear();
		CurrentLayout.OnChangeLayout();
		if(CurrentLayout) CurrentLayout.gameObject.SetActive(false);
		CurrentLayout = getLayout(layoutName);
		if(CurrentLayout == null) {
			Debug.LogError("Layout : '" + layoutName + "' does not exist.");
			return;
		}
		foreach (Actor actor in actors) { actor.gameObject.SetActive(false); }
	}

	public void ChangeActorLayoutPosition(string actorName, int positionIndex) {
		Actor actor = getActor(actorName);
		CurrentLayout.AddToLayout(positionIndex, actor.gameObject);
	}

	public void HideActors() {
		if (CurrentLayout)
			CurrentLayout.HideActors();
	}

	public void RevealActors() {
		if (CurrentLayout)
			CurrentLayout.RevealActors();
	}

	public void ClearChoices() {
		if (CurrentLayout) {
			CurrentLayout.ClearChoices();
		}
	}

	public void ShowInputField(UnityEngine.Events.UnityAction<string> callback) {
		InputField.gameObject.SetActive(true);
		InputField.onEndEdit.AddListener(callback); //  TODO Might not work if called several times
	}

	private Actor getActor(string name) {
		foreach (Actor actor in actors)
		{
			if (actor.ActorName.ToLower() == name.ToLower())
			{
				return actor;
			}
		}
		return null;
	}

	private DialogLayout getLayout(string name) {
		foreach (DialogLayout layout in layouts)
		{
			if (layout.Name.ToLower() == name.ToLower())
			{
				return layout;
			}
		}
		return null;
	}
}
