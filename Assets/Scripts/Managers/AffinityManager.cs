﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AffinityManager : Singleton<AffinityManager>
{
    void Awake() {
    }

    public void IncreaseAffinity(string name, int amount) {
        InkReader.Instance.UpdateVariableState("affinity_" + name.ToLower().Trim(), amount);
    }

    public int GetAffinity(string name) {
        return (int)InkReader.Instance.GetVariableState("affinity_" + name.ToLower().Trim());
    }
}
