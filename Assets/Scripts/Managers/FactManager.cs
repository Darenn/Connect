﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactManager : Singleton<FactManager>
{
    private Dictionary<string, bool> facts = new Dictionary<string, bool>();

    public void CreateFact(string factKey, bool factValue = false) 
    {
        if(facts.ContainsKey(factKey)) {
            Debug.LogError($"Trying to create an already created key '{factKey}'.");
        }
        else {
            facts.Add(factKey, factValue);
        }
    }

    /*
    * Returns false if the fact key does not exist.
    */
    public bool GetFactValue(string factKey) {
        if(!facts.ContainsKey(factKey)) {
            Debug.LogError($"Trying to get unknown fact '{factKey}'.");
            return false;
        }
        else {
            return facts[factKey];
        }
    }
}
