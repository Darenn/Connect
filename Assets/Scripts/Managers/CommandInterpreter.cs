﻿using System.Collections.Generic;
using UnityEngine;
using CommandTerminal;

public class CommandInterpreter : Singleton<CommandInterpreter>
{
    protected CommandInterpreter() {}

    public void ExecuteCommand(string command) {
        if (command.StartsWith(">>>"))
            command = command.Substring(3, command.Length - 3);
        else
            command = command.Substring(1, command.Length - 1);
        Terminal.Shell.RunCommand(command);
        if (Terminal.Shell.IssuedErrorMessage != null)
        {
            Debug.LogError("Command Shell: " + Terminal.Shell.IssuedErrorMessage);
        }
    }

    void Awake() {
       Terminal.Shell.AddCommand("say_hello", SayHello, 0, 0, "Say hello in the debug.");
       Terminal.Shell.AddCommand("change_room", ChangeRoom, 1, 1, "Change the current room.");
       Terminal.Shell.AddCommand("change_layout", ChangeLayout, 1, 7, "Change the dialog layout and add the characters given in order. You can put none if you want no actor on a given position.");
       Terminal.Shell.AddCommand("play_music", PlayMusic, 1, 3, "Play the given music. Arg 1 : music name. Arg 2 : isLooping. Arg 3 : delay");
       Terminal.Shell.AddCommand("stop_music", StopMusic, 0, 0, "Stop the music.");
       Terminal.Shell.AddCommand("play_sfx", PlaySFX, 1, 2, "Play the given sfx once. Arg 1 : sfx name. Arg 2 : delay");
       Terminal.Shell.AddCommand("hide_actors", HideActors, 0, 0, "Hide the actors.");
       Terminal.Shell.AddCommand("reveal_actors", RevealActors, 0, 0, "Reveal the actors.");
       Terminal.Shell.AddCommand("goto", GoToKnot, 1, 1, "Go to the specified path in the ink file.");
       Terminal.Shell.AddCommand("increase_affinity", IncreaseAffinity, 2, 2, "Increases from 'arg2' amount the affinity with the actor named 'arg1'.");
       Terminal.Shell.AddCommand("flash_camera", FlashCamera, 1, 1, "Flash the camera with the given duration in seconds.");
       Terminal.Shell.AddCommand("show_name_input_field", ShowNameInputField, 0, 0, "Show the input field for the player name.");
       Terminal.Shell.AddCommand("create_fact", CreateFact, 1, 2, "Create the fact with the given key and value.");
       
    }

    static public bool IsCommand(string txt) {
        return txt.StartsWith(">>>") || txt.StartsWith(">");
    }

    void SayHello(CommandArg[] args) {
        Debug.Log("Hello!");
    }

    void ChangeRoom(CommandArg[] args) {
        string roomName = args[0].String;
        GameManager.Instance.ChangeRoom(roomName);
    }

    void ChangeLayout(CommandArg[] args) {
        string layoutName = args[0].String;
        List<string> actors = new List<string>();
        for (int i = 1; i < args.Length; i++) {
            actors.Add(args[i].String);
        }
        DialogUIManager.Instance.ChangeLayout(layoutName);
        for (int i = 0; i < actors.Count; i++) {
            if (actors[i].ToLower() != "none") {
                DialogUIManager.Instance.ChangeActorLayoutPosition(actors[i], i);
            }
        }
    }

    void PlayMusic(CommandArg[] args) {
        string musicName = args[0].String;
        bool isLooping = true;
        float delay = 0;
        if(args.Length > 1) isLooping = args[1].Bool;
        if(args.Length > 2) delay = args[2].Float;
        SoundManager.Instance.PlayMusic(musicName, isLooping, delay);
    }

    void StopMusic(CommandArg[] args) {
        SoundManager.Instance.StopMusic();
    }

    void PlaySFX(CommandArg[] args) {    
        string sfxName = args[0].String;
        float delay = 0;
        if(args.Length > 1) delay = args[1].Float;
        SoundManager.Instance.PlaySFX(sfxName, delay);
    }

    void HideActors(CommandArg[] args) {
        DialogUIManager.Instance.HideActors();
    }

    void RevealActors(CommandArg[] args) {
        DialogUIManager.Instance.RevealActors();
    }

    void GoToKnot(CommandArg[] args) {
        string knot = args[0].String;
        InkReader.Instance.GoToKnot(knot);
    }

    void IncreaseAffinity(CommandArg[] args) {
        string name = args[0].String;
        int amount = args[1].Int;
        AffinityManager.Instance.IncreaseAffinity(name, amount);
    }

    void FlashCamera(CommandArg[] args) {
        float duration = args[0].Float; // in sec
        CameraEffectsManager.Instance.FlashCamera(duration);
    }

    void ShowNameInputField(CommandArg[] args) {
        GameManager.Instance.IsWaitingForInputField = true;
        DialogUIManager.Instance.InputField.Select();
        DialogUIManager.Instance.InputField.ActivateInputField();
        DialogUIManager.Instance.ShowInputField(delegate {GameManager.Instance.OnInputFieldSubmit(DialogUIManager.Instance.InputField);});
    }

    void CreateFact(CommandArg[] args) {
        Debug.Log("Created fact");
        string factKey = args[0].String;
        bool factValue = false;
        if(args.Length > 1)
            factValue = args[1].Bool;
        FactManager.Instance.CreateFact(factKey, factValue);
    }
}
