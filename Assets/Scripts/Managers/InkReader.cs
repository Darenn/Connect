﻿using UnityEngine;
using Ink.Runtime;

/*
* Read the ink story and give a decoded result usable by dialogs.
 */
public class InkReader : Singleton<InkReader>
{
	protected InkReader() {}

	void Awake() {
		story = new Story (inkJSONAsset.text);

		story.BindExternalFunction ("getFact", (string factKey) => {
     			return FactManager.Instance.GetFactValue(factKey);
	 		}
		);

		// INIT
		GoToKnot("init");
		while (story.canContinue) {
			story.Continue ();
		}
	}

	public class DecodedText {
		public DecodedText(string text) {
			Text = text;
			// Check for the actor name
			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] == ':') {
					ActorName = text.Substring(0, i);
					Text = text.Substring(i + 1, text.Length - (i + 1));
					break;
				}
			}

			// Check for the actor humor in actor name
			for (int i = 1; i < ActorName.Length; i++) {
				if (ActorName[i] == '(') {
					ActorHumor = ActorName.Substring(i+1, ActorName.Length-i-2);
					ActorName = ActorName.Substring(0, i).Trim();
				}
			}

			// Check for custom tags in Text
			string customTagsString = "";
			for (int i = 0; i < Text.Length; i++)
			{
				if (Text[i] == '$') {
					customTagsString = Text.Substring(i, Text.Length - i);
					Text = Text.Substring(0, i).Trim();
				}
			}
			string[] customTags = customTagsString.Split('$');
			CustomTags = new string[customTags.Length - 1];  // remove first null tab entry
			for (int i = 1; i < customTags.Length; i++)
			{
				CustomTags[i-1] = customTags[i].Trim();
			}
		}
		public string Text;
		public string ActorName = "";
		public string ActorHumor= "";
		public string[] CustomTags;
	}

	public class ReadingResult {
		public enum ResultType {Text, Choices, End, Command};
		public ReadingResult(DecodedText text) {
			Type = ResultType.Text;
			DialogEntry = text;
		}
		public ReadingResult(ResultType type = ResultType.End) {
			Type = type;
		}
		public ReadingResult(DecodedText[] choices) {
			Type = ResultType.Choices;
			Choices = choices;
		}
		public ReadingResult(string command) {
			Type = ResultType.Command;
			Command = command;
		}
		public ResultType Type;
		public DecodedText DialogEntry;
		public string Command;
		public DecodedText[] Choices;
	}

	public void GoToKnot(string knot) {
		story.ChoosePathString(knot);
	}

	private string storyPath = "/";

	public ReadingResult ContinueReading () {
		// Read one line of content
		if (story.canContinue) {
			string text = story.Continue ();
			if (story.state.currentPathString != null)
				storyPath = story.state.currentPathString;
			text = text.Trim();
			if (CommandInterpreter.IsCommand(text)) {
				return new ReadingResult(text);
			} else {
				ReadingResult result = new ReadingResult(new DecodedText(text));
				if (result.DialogEntry.ActorName == "")
					result.DialogEntry.ActorName = lastActor;
				else 
					lastActor = result.DialogEntry.ActorName;
				return result;
			}		
		}
		// Display all the choices, if there are any!
		else if(story.currentChoices.Count > 0) {
			DecodedText[] choices = new DecodedText[story.currentChoices.Count];
			for (int i = 0; i < story.currentChoices.Count; i++)
			{
				// TODO Debug.Log(storyPath + " " + i);
				choices[i] = new DecodedText(story.currentChoices[i].text);
			}
			foreach (string tag in story.currentTags)
			{
				if (CommandInterpreter.IsCommand(tag))
					CommandInterpreter.Instance.ExecuteCommand(tag);
			}
			return new ReadingResult(choices);
		}
		// If we've read all the content and there's no choices, the story is finished!
		else {
			DialogUIManager.Instance.OnStopDialog();
			return new ReadingResult(ReadingResult.ResultType.End);
		}
	}

	/*
	* Tells the story the choice we choose
	 */
	public void ChooseChoice(int index) {
		story.ChooseChoiceIndex (index);
	}

	public void UpdateVariableState(string variable, int value) {
		story.variablesState[variable] = value;
	}

	public void UpdateVariableState(string variable, string value) {
		story.variablesState[variable] = value;
	}

	public object GetVariableState(string variable) {
		return story.variablesState[variable];
	}

	public VariablesState GetVariables() {
		return story.variablesState;
	}

	

	[SerializeField] private TextAsset inkJSONAsset = default;
	private Story story;
	private string lastActor;
}
