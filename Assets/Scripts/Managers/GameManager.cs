﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : Singleton<GameManager>
{
    public Calendar calendar;
    public Day CurrentDay;
    public GameObject StartingRoom;
    public string StartingKnot = "start";
    public string PlayerName {
        get {
            return InkReader.Instance.GetVariables()["p"].ToString();
        }
        set {
            InkReader.Instance.UpdateVariableState("p", PlayerName);
        }
    }

    public delegate void OnDialogAction();
    public static event OnDialogAction OnDialogStart = delegate {};
    public static event OnDialogAction OnDialogEnd = delegate {};
    public bool IsTalking { get; private set;}
    public bool IsWaitingForInputField { get; set;}

    [HideInInspector] public GameObject CurrentRoom;

    [SerializeField] private GameObject roomContainer = default;
    
    bool isWaitingForStartingNewDay = false;
    int remainingActivityPoints;
    private GameObject[] rooms;

    public void ConsumeActivityPoint(int amount = 1) {
        remainingActivityPoints--;
        if (remainingActivityPoints <= 0) {
            GoToNextDay();
        }
    }

    public void GoToNextDay() {
        remainingActivityPoints = 1;
        CurrentDay = calendar.GetNextDay(CurrentDay);
        UIManager.Instance.DisplayCalendar(calendar, CurrentDay);
        isWaitingForStartingNewDay = true;
    }

    public void StartNewDay() {
        isWaitingForStartingNewDay = false;
        UIManager.Instance.HideCalendar();
        ChangeRoom(StartingRoom.name);
    }

    void Awake() {
        rooms = new GameObject[roomContainer.transform.childCount];
		for (int i = 0; i < rooms.Length; i++)
		{
			rooms[i] = roomContainer.transform.GetChild(i).gameObject;
		}
    }

    void Start() {
        for (int i = 0; i < rooms.Length; i++)
		{
			rooms[i].gameObject.SetActive(false);
		}
        ReadDialog(StartingKnot);
    }

    void Update() {
        if (isWaitingForStartingNewDay && Input.GetKeyDown(KeyCode.Space)) {
            StartNewDay();
        }
        if (IsTalking && (Input.GetKeyDown(KeyCode.Space))) {
			ContinueDialog();
		}
    }

    public void ReadDialog(string knot) {
        InkReader.Instance.GoToKnot(knot);
        IsTalking = true;
        OnDialogStart();
        ContinueDialog();
    }

    void ContinueDialog() {
        InkReader.ReadingResult result = InkReader.Instance.ContinueReading(); // TODO NOOOO it push buttons all over when spacingduring a choice
            switch (result.Type) {
                case InkReader.ReadingResult.ResultType.Text:
                    DialogUIManager.Instance.DisplayText(result.DialogEntry.Text, result.DialogEntry.ActorName, result.DialogEntry.ActorHumor);
                    executeTags(result.DialogEntry.CustomTags);
                break;
                case InkReader.ReadingResult.ResultType.Choices:
                    DialogUIManager.Instance.ClearChoices();
                    for (int i = 0; i < result.Choices.Length; i++)
                    {
                        CreateChoiceButton(result.Choices[i].Text, i);
                    }
                break;
                case InkReader.ReadingResult.ResultType.Command:
                    CommandInterpreter.Instance.ExecuteCommand(result.Command);
                    if(!IsWaitingForInputField) {
                        ContinueDialog(); // command not considered as entry of text.
                    }
                    break;
                case InkReader.ReadingResult.ResultType.End:
                    OnDialogEnd();
                    IsTalking = false;
                break;
                default:
                    Debug.LogError("Didn't do anything from the reading results!");
                    break;
            }
    }

    void executeTags(string[] tags) {
        if (tags == null) return;
        foreach (string tag in tags)
        {
            if (CommandInterpreter.IsCommand(tag)) {
                CommandInterpreter.Instance.ExecuteCommand(tag);
            }
        }
    }



    // When we click the choice button, tell the story to choose that choice!
	void OnClickChoiceButton (int index) {
		InkReader.Instance.ChooseChoice(index);
		ContinueDialog();
	}

    void CreateChoiceButton(string text, int index) {
        DialogUIManager.Instance.CreateChoiceButton(text, delegate {OnClickChoiceButton (index);});
    }

    
	public void ChangeRoom(string roomName) {
		if (CurrentRoom != null) {
			CurrentRoom.gameObject.SetActive(false);
			CurrentRoom = null;
		}
		foreach (GameObject room in rooms)
		{
			if(room.name == roomName) {
				room.gameObject.SetActive(true);
				CurrentRoom = room;
			}
		}
		if(CurrentRoom == null) {
			Debug.LogError("Room : '" + roomName + "' does not exist.");
		}
	}

    public void OnInputFieldSubmit(TMP_InputField inputField) { // TODO to refactorize to allow use of more inpute field later
        IsWaitingForInputField = false;
        PlayerName = inputField.text;
        InkReader.Instance.UpdateVariableState("p", PlayerName);
        inputField.gameObject.SetActive(false);
        ContinueDialog();
    }
}
