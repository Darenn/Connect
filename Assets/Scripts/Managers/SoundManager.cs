﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    protected SoundManager() {}

    
    [System.Serializable]
    public class Music {
        public string Name;
        public AudioClip MusicClip;
    }

    public AudioSource MusicAudioSource;
    public GameObject SFXAudioSourceContainer;
    public Music[] MusicClips;
    public AudioClip[] SFXClips;


    public void PlayMusic(string name, bool isLooping = true, float delay = 0) {
        AudioClip clip = null;
        foreach (Music music in MusicClips)
        {
            if(music.Name == name) {
                clip = music.MusicClip;
                break;
            }
        }
        if(!clip) {
            Debug.LogError("No audio clip '" + name + "'.");
            return;
        }
        MusicAudioSource.clip = clip;
        MusicAudioSource.PlayDelayed(delay);
        MusicAudioSource.loop = isLooping;
    }

    public void StopMusic() {
        MusicAudioSource.Stop();
    }

    public void PlaySFX(string name, float delay = 0) {
        GameObject SFXGameObject = new GameObject();
        SFXGameObject.transform.parent = SFXAudioSourceContainer.transform;
        AudioSource SFXAudioSource = SFXGameObject.AddComponent<AudioSource>();
        foreach (AudioClip clip in SFXClips)
        {
            if(clip.name == name) {
                SFXAudioSource.clip = clip;
                break;
            }
        }
        SFXAudioSource.PlayDelayed(delay);
        Destroy(SFXGameObject, SFXAudioSource.clip.length);
    }
}
