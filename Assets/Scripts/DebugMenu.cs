﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class DebugMenu : MonoBehaviour
{
    public string InkFilesPath;

    private int mainToolBarID = 0;
    private int variablesToolBarID = 0;
    private string[] mainToolbarStrings = {"Knots", "Variables"};
    private string[] variablesToolbarStrings = {"All", "String", "Int", "Boolean"};
    private List<Tuple<string, List<string>>> knots = new List<Tuple<string, List<string>>>();
    
    private List<String> StringVariables = new List<string>();
    private List<String> IntVariables = new List<String>();
    private List<String> BoolVariables = new List<String>();
    private Vector2 scrollViewVector = Vector2.zero;
    private bool isOpen = false; // Is the menu opened?
 
    void Start () {
        ComputeKnots();
    }

    void ComputeKnots() {
        var info = new DirectoryInfo(InkFilesPath);
        var fileInfo = info.GetFiles("*.ink", SearchOption.AllDirectories);
        foreach(var InkFile in fileInfo) {
            using (StreamReader streamReader = InkFile.OpenText()) {
                using (StringReader reader = new StringReader(streamReader.ReadToEnd()))
                {
                    // Loop over the lines in the string.
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if(line.StartsWith("==")) {
                            line = line.Trim('=');
                            line = line.Trim(' ');
                            knots.Add(new Tuple<string, List<string>>(line, new List<string>()));
                        }
                        else if(line.StartsWith("=")) {
                            line = line.Trim('=');
                            line = line.Trim(' ');
                            knots[knots.Count - 1].Item2.Add(line);
                        }
                    }
                }
            }
        }
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Menu)) {
            isOpen = !isOpen;
        }
    }

    void OnGUI () 
    {
        if(!isOpen) return;
        mainToolBarID = GUILayout.Toolbar (mainToolBarID, mainToolbarStrings);

        switch(mainToolBarID) {
            case 0:
            OnKnots();
            break;
            case 1:
            OnVariables();
            break;
        }   
    }

    void OnKnots() {
        scrollViewVector = GUILayout.BeginScrollView (scrollViewVector);
            foreach(var tuple in knots) {
                GUILayout.BeginHorizontal();
                var knot = tuple.Item1;
                if (GUILayout.Button(knot)) {
                    GameManager.Instance.ReadDialog(knot);
                }              
                foreach (var stich in tuple.Item2)
                {
                    if(GUILayout.Button(stich)) {
                        GameManager.Instance.ReadDialog(knot + '.' + stich);
                    }
                }
                GUILayout.EndHorizontal();
            } 
        GUILayout.EndScrollView();
    }

    void OnVariables() {
        FillLists();
        variablesToolBarID = GUILayout.Toolbar (variablesToolBarID, variablesToolbarStrings);
        scrollViewVector = GUILayout.BeginScrollView (scrollViewVector);
        switch(variablesToolBarID) {
            case 0:
            DisplayAllEditableVariables();
            break;
            case 1:
            DisplayEditableStringVariables();
            break;
            case 2:
            DisplayEditableIntVariables();
            break;
            case 3:
            DisplayEditableBoolVariables();
            break;
        }   
        GUILayout.EndScrollView();
    }

    void DisplayAllEditableVariables() {
        DisplayEditableStringVariables();
        DisplayEditableIntVariables();
        DisplayEditableBoolVariables();
    }

    void DisplayEditableStringVariables() {
        foreach (var variable in StringVariables)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(variable);
            InkReader.Instance.GetVariables()[variable] = GUILayout.TextField((string)InkReader.Instance.GetVariables()[variable]);
            GUILayout.EndHorizontal();
        }
    }

    void DisplayEditableIntVariables() {
        foreach (var variable in IntVariables)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(variable);
            string newValue = GUILayout.TextField(InkReader.Instance.GetVariables()[variable].ToString());
            int newInt = 0;
            Int32.TryParse(newValue, out newInt);
            InkReader.Instance.GetVariables()[variable] = newInt;
            GUILayout.EndHorizontal();
        }
    }

    void DisplayEditableBoolVariables() {
        foreach (var variable in BoolVariables)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(variable);
            bool newValue = GUILayout.Toggle(Convert.ToBoolean(InkReader.Instance.GetVariables()[variable]), "");
            InkReader.Instance.GetVariables()[variable] = newValue;
            GUILayout.EndHorizontal();
        }
    }

    void FillLists() {
        StringVariables.Clear();
        IntVariables.Clear();
        BoolVariables.Clear();
        List<String> variableStrings = new List<String>();
        foreach (var variable in InkReader.Instance.GetVariables())
        {
            variableStrings.Add(variable);
        }
        foreach (var variable in variableStrings)
        {
            var variableValue = InkReader.Instance.GetVariables()[variable];
            if (variable.StartsWith("b_")) {
                BoolVariables.Add(variable);
            }
            else if(variableValue is string) {
                StringVariables.Add(variable);
            }
            else if (variableValue is int) {

                IntVariables.Add(variable);
            }
        }
    }
}
