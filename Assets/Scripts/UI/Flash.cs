﻿ using UnityEngine;
 using UnityEngine.UI;
 using System.Collections;
 using DG.Tweening;
 
 [RequireComponent(typeof(Image))]
 public class Flash : MonoBehaviour {
     private Image flashImage;
     private bool flashing = false;
 
    void Start()
    {
        flashImage = GetComponent<Image>();
        ChangeTransparency(flashImage, 0);
    }
 
    public void CameraFlash(float FlashTimeLength)
    {
        if(flashing) return;
        flashing = true;
        ChangeTransparency(flashImage, 1);
        flashImage.DOFade(0, FlashTimeLength).OnComplete(OnFlashCompleted);
    }

    private void OnFlashCompleted() {
        flashing = false;
    }

    private void ChangeTransparency(Image i, float a) {
        Color col = i.color;
        col.a = a;
        i.color = col;
    }
 }
    
 