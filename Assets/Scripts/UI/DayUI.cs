﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class DayUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Transform IconPosition = default;

    [SerializeField] private TMP_Text dayNumber = default;
    [SerializeField] private GameObject questIconPrefab = default;
    [SerializeField] private GameObject highlightDateGameObject = default;

    public CalendarUI calendarUI;
    private Day assignedDay;

    public void AssignDay(Day d) {
        assignedDay = d;
        dayNumber.text = d.DayNumber.ToString();
        if (d.Quests.Length > 0) {
            Instantiate(questIconPrefab, IconPosition);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        calendarUI.DisplayDetails(assignedDay);
        highlightDateGameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        highlightDateGameObject.SetActive(false);
    }
}
