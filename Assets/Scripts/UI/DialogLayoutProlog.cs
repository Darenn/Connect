﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using System.Collections;

public class DialogLayoutProlog: DialogLayout
{
    public float TimeBetweenEachLetter;

    [SerializeField] private CanvasRenderer choicesPanel = default;
    [SerializeField] private TMP_Text storyText = default;
	[SerializeField] private TMP_Text actorNameText = default;
	[SerializeField] private Button buttonPrefab = default;

    override protected void Awake() {
    }

    override public void AddToLayout(int positionIndex, GameObject go) {
        
    }

    override public Button CreateChoiceButton(string choice, UnityEngine.Events.UnityAction callback) {
			Button choiceButton = Instantiate (buttonPrefab);
			choiceButton.transform.SetParent (choicesPanel.transform, false);
			
			// Gets the text from the button prefab
			TMP_Text choiceText = choiceButton.GetComponentInChildren<TMP_Text> ();
            string[] c = choice.Split(':');
            if (c.Length > 1) {
                choice = c[1];
            }
			choiceText.text = choice.Trim ();

			// Make the button expand to fit the text
			HorizontalLayoutGroup layoutGroup = choiceButton.GetComponent <HorizontalLayoutGroup> ();
			layoutGroup.childForceExpandHeight = false;
            choiceButton.onClick.AddListener (callback);
            return choiceButton;
    }

    override public void DisplayText(string text, Actor actor) {
		ClearText();
		ClearName();
        ClearChoices();
        lastActorTalking = actor;
        actor.Talk(text);
        string actorName = actor.ActorName.Substring(0,1).ToUpper() + actor.ActorName.Substring(1, actor.ActorName.Length - 1);
        if (actor.name.ToLower() == "p") {
            actorName = GameManager.Instance.PlayerName.Substring(0,1).ToUpper() + GameManager.Instance.PlayerName.Substring(1, actor.ActorName.Length - 1).ToLower();
        }
		actorNameText.text = actorName;
        if(textCoroutine != null) StopCoroutine(textCoroutine);
        textCoroutine = DisplayTextProgressively(text);
        StartCoroutine(textCoroutine);
	}

     override public void DisplayDescText(string text) {
        ClearText();
		ClearName();
        ClearChoices();
        Unclear();
        if(textCoroutine != null) StopCoroutine(textCoroutine);
        textCoroutine = DisplayTextProgressively(text);
        StartCoroutine(textCoroutine);
    }

    override public void Clear() {
		ClearCharacter();
		ClearText();
		ClearName();
        ClearChoices();
	}

    override public void Unclear() {
    }

	public void ClearCharacter() {

	}

	public void ClearName() {
		actorNameText.text = "";
	}

	public void ClearText() {
		storyText.text = "";
	}
	
    public override void ClearChoices() {

    }

    override public void HideActors() {

    }

    override public void RevealActors() {

    }

    private Actor lastActorTalking;
    private IEnumerator textCoroutine;

    private IEnumerator DisplayTextProgressively(string text) {
        int i = 0;
        while(i <= text.Length) {
            storyText.text = text.Substring(0, i);
            i++;
            yield return new WaitForSeconds(TimeBetweenEachLetter);
        }
    }
}
