﻿using UnityEngine;
using UnityEngine.UI;

public abstract class DialogLayout : MonoBehaviour
{
    public string Name;
    [SerializeField] protected Transform[] positionsTransforms;

    public abstract void AddToLayout(int positionIndex, GameObject go);
    public abstract Button CreateChoiceButton(string choice, UnityEngine.Events.UnityAction callback);
    public abstract void DisplayText(string text, Actor actor);
    public abstract void DisplayDescText(string text);
    public abstract void Clear();
    public abstract void Unclear();
    public abstract void HideActors();
    public abstract void RevealActors();
    public abstract void ClearChoices();
    public void OnChangeLayout() {
        RevealActors();
    }

    protected abstract void Awake();
}
