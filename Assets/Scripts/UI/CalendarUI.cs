﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class CalendarUI : MonoBehaviour
{
    [SerializeField] private GameObject Grid = default;
    [SerializeField] private TMP_Text MonthText = default;
    [SerializeField] private TMP_Text DetailsDate = default;
    [SerializeField] private TMP_Text DetailsQuestText = default;
    [SerializeField] private GameObject PlayerIcon = default;

    private DayUI[] days;

    private void Awake() {
        days = new DayUI[Grid.transform.childCount];
        for (int i = 0; i < Grid.transform.childCount; i++)
        {
            days[i] = Grid.transform.GetChild(i).GetComponent<DayUI>();
            days[i].calendarUI = this;
        }
    }

    public void DisplayDetails(Day day) {
        DetailsDate.text = day.DayNumber + "/" + day.Month.MonthNumber;
        if (day.Quests.Length > 0) {
             foreach (var quest in day.Quests)
             {
                 DetailsQuestText.text = "";
                 DetailsQuestText.text += quest.Title + "\n";
             }
        }
        else {
            DetailsQuestText.text = "No quest";
        }
    }

    public void DisplayCalendar(Calendar c, int monthNumber, int oldDay, int newDay) {
        Month month = c.months[monthNumber];
        for (int i = 0; i < month.days.Length; i++)
        {
            days[i].AssignDay(month.days[i]);
        }
        MonthText.text = month.MonthName.ToString();
        LaunchPlayerAnimation(days[oldDay-1].IconPosition.transform.position, days[newDay-1].IconPosition.transform.position);
    }

    public void LaunchPlayerAnimation(Vector2 oldDayPosition, Vector2 newDayPosition) {
        PlayerIcon.transform.position = oldDayPosition;
        PlayerIcon.transform.DOMove(newDayPosition, 2);
    }
}
