﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using System.Collections;

public class DialogLayoutGroup : DialogLayout
{
    public Color notTalkingColor;
    public float TimeBetweenEachLetter;

    [SerializeField] private CanvasRenderer choicesPanel = default;
    [SerializeField] private TMP_Text storyText = default;
	[SerializeField] private TMP_Text actorNameText = default;
    [SerializeField] private GameObject actorNamePanel = default;
	[SerializeField] private Button buttonPrefab = default;

    private List<GameObject> actors;

    override protected void Awake() {
        actors = new List<GameObject>();
        for (int i = 0; i < positionsTransforms.Length; i++)
        {
            actors.Add(null);
        }
    }

    override public void AddToLayout(int positionIndex, GameObject go) {
        if (positionIndex >= positionsTransforms.Length) {
            Debug.LogError("No position '" + positionIndex + "' in layout '" + Name + "'");
            return;
        }
        if (actors[positionIndex] != null) {
            actors[positionIndex].gameObject.SetActive(false);
        }
        ChangeColorToNotTalking(go.GetComponent<Actor>()); // TODO added dependencie to actor
        actors[positionIndex] = go;
        go.SetActive(true);
        Transform positionTransform = positionsTransforms[positionIndex];
        go.transform.position = positionTransform.position;
        go.transform.localScale = positionTransform.localScale;
    }

    override public Button CreateChoiceButton(string choice, UnityEngine.Events.UnityAction callback) {
			Button choiceButton = Instantiate (buttonPrefab);
			choiceButton.transform.SetParent (choicesPanel.transform, false);
			
			// Gets the text from the button prefab
			TMP_Text choiceText = choiceButton.GetComponentInChildren<TMP_Text> ();
            string[] c = choice.Split(':');
            if (c.Length > 1) {
                choice = c[1];
            }
			choiceText.text = choice.Trim ();

			// Make the button expand to fit the text
			HorizontalLayoutGroup layoutGroup = choiceButton.GetComponent <HorizontalLayoutGroup> ();
			layoutGroup.childForceExpandHeight = false;
            choiceButton.onClick.AddListener (callback);
            return choiceButton;
    }

    override public void DisplayText(string text, Actor actor) {
		ClearText();
		ClearName();
        ClearChoices();
        Unclear();
        if(actorNamePanel) actorNamePanel.SetActive(true);
        if (lastActorTalking) ChangeColorToNotTalking(lastActorTalking);
        ResetColor(actor);
        lastActorTalking = actor;
        actor.Talk(text);
        string actorName = actor.ActorName.Substring(0,1).ToUpper() + actor.ActorName.Substring(1, actor.ActorName.Length - 1);
        if (actor.ActorName.ToLower() == "p") {
            actorName = GameManager.Instance.PlayerName.Substring(0,1).ToUpper() + GameManager.Instance.PlayerName.Substring(1, GameManager.Instance.PlayerName.Length - 1).ToLower();
        }
		actorNameText.text = actorName;
        if(textCoroutine != null) StopCoroutine(textCoroutine);
        textCoroutine = DisplayTextProgressively(text);
        StartCoroutine(textCoroutine);
	}

    override public void DisplayDescText(string text) {
        ClearText();
		ClearName();
        ClearChoices();
        Unclear();
        if(actorNamePanel) actorNamePanel.SetActive(false);
        if (lastActorTalking) ChangeColorToNotTalking(lastActorTalking);
        if(textCoroutine != null) StopCoroutine(textCoroutine);
        textCoroutine = DisplayTextProgressively(text);
        StartCoroutine(textCoroutine);
    }

    override public void Clear() {
		ClearCharacter();
		ClearText();
		ClearName();
        ClearChoices();
	}

    override public void Unclear() {
        foreach (GameObject actor in actors)
		{
            if (actor)
			    actor.SetActive(true);
		}
    }

	public void ClearCharacter() {
		foreach (GameObject actor in actors)
		{
            if (actor)
			    actor.SetActive(false);
		}
	}

	public void ClearName() {
		actorNameText.text = "";
	}

	public void ClearText() {
		storyText.text = "";
	}
	
    public override void ClearChoices() {
        int childCount = choicesPanel.transform.childCount;
		for (int i = childCount - 1; i >= 0; --i) {
			GameObject.Destroy (choicesPanel.transform.GetChild (i).gameObject);
		}
    }

    override public void HideActors() {
        foreach (GameObject actor in actors)
        {
            if(!actor) continue;
            foreach (SpriteRenderer sr in actor.GetComponentsInChildren<SpriteRenderer>())
            {
                sr.enabled = false;
            }
        }
    }

    override public void RevealActors() {
        foreach (GameObject actor in actors)
        {
            if(!actor) continue;
            foreach (SpriteRenderer sr in actor.GetComponentsInChildren<SpriteRenderer>())
            {
                sr.enabled = true;
            }
        }
    }

    private Actor lastActorTalking;
    private IEnumerator textCoroutine;

    private void ChangeColorToNotTalking(Actor actor) {
        foreach (SpriteRenderer sr in actor.GetComponentsInChildren<SpriteRenderer>())
        {
            sr.color = notTalkingColor;
        }
    }

    private void ResetColor(Actor actor) {
        foreach (SpriteRenderer sr in actor.GetComponentsInChildren<SpriteRenderer>())
        {
            sr.color = Color.white;
        }
    }

    private IEnumerator DisplayTextProgressively(string text) {
        int i = 0;
        while(i <= text.Length) {
            storyText.text = text.Substring(0, i);
            i++;
            yield return new WaitForSeconds(TimeBetweenEachLetter);
        }
    }
}
