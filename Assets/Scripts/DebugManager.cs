﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour
{

    [SerializeField] private string StartingKnot = "start";

    void Start()
    {
        #if UNITY_EDITOR
            GameManager.Instance.StartingKnot = StartingKnot;
        #endif
    }

    void Update()
    {
        
    }
}
