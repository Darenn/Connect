﻿using System.Collections;
using UnityEngine;

/*
* \brief An actor gameobject that will be used to display character (both in and out of dialogues).
 */
public class Actor : MonoBehaviour
{
    /*
    * Character sprites for one humor/emotion.
     */
    [System.Serializable]
    public class HumorSprite {
        public string Humor;
        public Sprite Sprite;
        public Sprite SpriteEyesOpened;
        public Sprite SpriteEyesClosed;
        public Sprite SpriteMouthOpened;
        public Sprite SpriteMouthClosed;
    }

    public string ActorName;
    public SpriteRenderer BaseSpriteRenderer;
    public string StartingHumor;
    public HumorSprite[] HumorSprites;
    [SerializeField] TMPro.TMP_Text affinityText = default;

    [Header("Eyes Animation")]
    [SerializeField] private SpriteRenderer eyesSpriteRenderer = default;
    public float TimeBetweenBlinkMin;
    public float TimeBetweenBlinkMax;
    public float BlinkTime;
    public float DoubleBlinkProbability;
    [Header("Mouth Animation")]
    [SerializeField] private SpriteRenderer mouthSpriteRenderer = default;
    public int SyllabeLength;
    public float TimeToSayASyllabe;
    public float PauseTimeBetweenSyllabeMin;
    public float PauseTimeBetweenSyllabeMax;
    public float PauseTimeBetweenWordsMin;
    public float PauseTimeBetweenWordsMax;

   
    public string Humor {
        get {
            return humor.Humor;
        }
        set {
            humor = getSpriteHumor(value);
            BaseSpriteRenderer.sprite = humor.Sprite;
            eyesSpriteRenderer.sprite = humor.SpriteEyesOpened;
            mouthSpriteRenderer.sprite = humor.SpriteMouthClosed;
        }
    }

    /*
    * Current humor of the actor.
    */
    private HumorSprite humor;
    /*
    * Restarted each time the character talks.
     */
    private IEnumerator talkingCoroutine;

    void Awake() {
        Humor = StartingHumor.ToLower();
        ActorName = ActorName.ToLower();
    }

    void OnEnable() {
        StartCoroutine(blinkEyes());
    }

    void Update() {
        affinityText.text = AffinityManager.Instance.GetAffinity(ActorName).ToString();
    }

    public void Talk(string text) {
        if(talkingCoroutine != null) StopCoroutine(talkingCoroutine);
        talkingCoroutine = talk(text);
        if(gameObject.activeSelf)
            StartCoroutine(talkingCoroutine);
    }

    private HumorSprite getSpriteHumor(string humor) {
        humor = humor.ToLower();
        foreach (HumorSprite humorSprite in HumorSprites)
        {
            if (humorSprite.Humor.ToLower() == humor) {
                return humorSprite;
            }
        }
        Debug.LogError("No sprite for humor '" + humor + "' on actor '" + ActorName + "'.");
        return null;
    }

    IEnumerator blinkEyes() {
        while (true) {
            yield return new WaitForSeconds(Random.Range(TimeBetweenBlinkMin, TimeBetweenBlinkMax));
            eyesSpriteRenderer.sprite = humor.SpriteEyesClosed;
            if (Random.Range(0.0f, 1.0f) <= DoubleBlinkProbability) {
                yield return new WaitForSeconds(BlinkTime);
                eyesSpriteRenderer.sprite = humor.SpriteEyesOpened;
                yield return new WaitForSeconds(BlinkTime);
                eyesSpriteRenderer.sprite = humor.SpriteEyesClosed;
                yield return new WaitForSeconds(BlinkTime);
                eyesSpriteRenderer.sprite = humor.SpriteEyesOpened;
            }
            else {
                yield return new WaitForSeconds(BlinkTime);
                eyesSpriteRenderer.sprite = humor.SpriteEyesOpened;
            }
        }
    }

    /*
    * Makes the mouth moving depending of the text.
     */
    IEnumerator talk(string text) {
        string[] sentence = text.Split(' ');
        foreach (string word in sentence)
        {
            for (int numberSyllabe = 0; numberSyllabe < word.Length/SyllabeLength; numberSyllabe++)
            {
                mouthSpriteRenderer.sprite = humor.SpriteMouthOpened;
                yield return new WaitForSeconds(TimeToSayASyllabe);
                mouthSpriteRenderer.sprite = humor.SpriteMouthClosed;
                // Wait if it's not the last syllabe
                if (numberSyllabe != word.Length - SyllabeLength) { 
                    yield return new WaitForSeconds(Random.Range(PauseTimeBetweenSyllabeMin, PauseTimeBetweenSyllabeMax));
                }
            }
            yield return new WaitForSeconds(Random.Range(PauseTimeBetweenWordsMin, PauseTimeBetweenWordsMax));
        }
    }
}
