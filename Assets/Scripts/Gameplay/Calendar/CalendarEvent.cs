﻿using UnityEngine;

/*
* A CalendarEvent is a piece of code to be executed on the start of a specific day.
* To do so, it should be a child of this specific day.
 */
public abstract class CalendarEvent : MonoBehaviour
{
    public delegate void CalendarEventAction(CalendarEvent calendarEvent);
    public event CalendarEventAction OnCalendarEventStart = delegate {};
    public event CalendarEventAction OnCalendarEventEnd = delegate {};

    public void RunEvent() {
        OnCalendarEventStart(this);
        ExecuteEvent();
        OnCalendarEventEnd(this);
    }

    /*
    * \brief Override this and put your event code in this method.
    */
    protected abstract void ExecuteEvent();
}
