﻿using UnityEngine;

public class Day : MonoBehaviour
{
    public enum WeekDay {Monday, Tuesday, Wednesday, Thirsday, Friday, Saturday, Sunday};

    public int DayNumber;
    public Month Month;
    public WeekDay DayName;

    public Quest[] Quests;

}
