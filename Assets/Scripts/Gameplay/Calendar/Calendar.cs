﻿using UnityEngine;

/*
* A calendar with months and days.
* Months must be children of this gameobject.
* During edit mode, change names of its children.
 */
 [ExecuteInEditMode]
public class Calendar : MonoBehaviour
{
    public Month[] months;

    void Awake() {
        months = new Month[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            Month m = transform.GetChild(i).GetComponent<Month>();
            if (m)
                months[i] = m;
        }
    }

    public Day GetNextDay(Day day) {
        for (int i = 0; i < months.Length; i++)
        {
            for (int j = 0; j < months[i].days.Length; j++)
            {
                Day d = months[i].days[j];
                if (day == d) {
                    if (j + 1 > months[i].days.Length) {
                        return months[i + 1].days[0];
                    } else {
                        return months[i].days[j + 1];
                    }
                }
            }
        }
        Debug.LogError("No day was found after the day '" + day + "'.");
        return null;
    }

    void Update() {
        foreach (var month in months)
        {
            foreach (var day in month. days)
            {
                day.name = day.DayName + " " + day.DayNumber + "/" + month.MonthNumber;
            }
        }
    }
}
