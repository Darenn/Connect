﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
* A Month should be child of a Calendar.
* Should have Days as children.
 */
public class Month : MonoBehaviour
{
    public enum YearMonth {January, February, Marsh, April, May, June, July, August, September, October, November, December};

    public int MonthNumber;
    public YearMonth MonthName;
    public Day[] days;

    void Awake() {
        days = new Day[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            Day d = transform.GetChild(i).GetComponent<Day>();
            if (d) {
                days[i] = d;
                d.Month = this;
            }
        }
    }
}
