﻿using UnityEngine;

/*
* An activity is a piece of code that will be executed at runtime.
* An activity consumes 1 activity point after it's execution.
*/
public abstract class Activity : ScriptableObject
{
    public string Title;
    public string Actor;

    public delegate void ActivityAction(Activity activity);
    public event ActivityAction OnActivityStart = delegate {};
    public event ActivityAction OnActivityEnd = delegate {};

    public virtual bool IsAvailable() {
        return true;
    }
    
    public void RunActivity() {
        OnActivityEnd += onActivityEnd;
        OnActivityStart(this);
        StartActivity();
    }

    /*
    * \brief Must be called at the end of the activity. 
    */
    private void onActivityEnd() {
        GameManager.Instance.ConsumeActivityPoint();
    }

    /*
    * \brief Override this and put your activity code in this method.
    * Don't forget to call TriggerOnActivityEndEvent!
    */
    protected abstract void StartActivity();

    protected void TriggerOnActivityEndEvent() {
        OnActivityEnd(this);
    }

    private void onActivityEnd(Activity a) {
        GameManager.Instance.ConsumeActivityPoint();
        OnActivityEnd -= onActivityEnd;
    }
}
