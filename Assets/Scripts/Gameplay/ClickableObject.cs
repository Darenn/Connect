﻿using UnityEngine;

/*
* Put it on any clickable object in the scene.
* The mouse cursor will change on hovering.
 */
public class ClickableObject : MonoBehaviour
{
     /*
     * The pointer texture when mouse is hovering.
      */
    public Texture2D PointerTexture;

    void OnMouseEnter() {
         if (GameManager.Instance.IsTalking) return;
         Cursor.SetCursor(PointerTexture, Vector2.zero, CursorMode.Auto);
    }

    void OnMouseExit() {
         if (GameManager.Instance.IsTalking) return;
         Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}
