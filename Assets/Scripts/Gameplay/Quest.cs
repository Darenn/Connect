﻿using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = "Connect/Create Quest", order = 1)]
public class Quest : Activity
{
    public string Knot;

    private bool isDone = false;

    public override bool IsAvailable() {
        return !isDone;
    }

    protected override void StartActivity() {
        isDone = true;
        GameManager.Instance.ReadDialog(Title);
        GameManager.OnDialogEnd += OnDialogEnd;
    }

    private void OnDialogEnd() {
        GameManager.OnDialogEnd -= OnDialogEnd;
        TriggerOnActivityEndEvent();
    }
}
