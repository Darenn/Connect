﻿using UnityEngine;

/*
* Put it on an actor to be able to click on it and start a dialogue with him.
 */
 [RequireComponent(typeof(Actor))]
 [RequireComponent(typeof(ClickableObject))]
public class InteractableActor : MonoBehaviour
{
    private string actorName;
    private bool isMouseOver = false;

    void Awake() {
        actorName = GetComponent<Actor>().ActorName;
    }

    void OnMouseDown() {
        if (GameManager.Instance.IsTalking) return;
        if (!isMouseOver) return;
        CommandInterpreter.Instance.ExecuteCommand(">>> change_layout layout_intimate " + actorName);
        GameManager.Instance.ReadDialog(actorName + "_ask");
        DialogUIManager.Instance.CreateChoiceButton("Nothing", delegate {OnChooseNothing(0);});
        Activity[] activities = GameManager.Instance.CurrentDay.Quests;
        if (activities.Length > 0) {
            for (int i = 0; i < activities.Length; i++)
            {
                Activity activity = activities[i];
                if (activity.Actor.ToLower() == actorName.ToLower()) {
                    CreateChoiceButton(activity.Title, i);
                }
            }
        }
    }

    void CreateChoiceButton(string name, int index) {
        DialogUIManager.Instance.CreateChoiceButton(name, delegate {RunActivity(index);});
	}

    void RunActivity(int index) {
        GameManager.Instance.CurrentDay.Quests[index].RunActivity();
    }

    void OnChooseNothing(int index) {
        GameManager.Instance.ReadDialog(actorName + "_talk");
    }

    void OnMouseEnter() {
         isMouseOver = true;
    }

    void OnMouseExit() {
         isMouseOver = false;
    }
}
